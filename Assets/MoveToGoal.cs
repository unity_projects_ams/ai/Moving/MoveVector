﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToGoal : MonoBehaviour
{
    [SerializeField] Transform goal;
    [SerializeField] float speed = 2f;
    [SerializeField] float accuracy = 0.25f;

    void Start()
    {
    }

    void LateUpdate()
    {
        transform.LookAt(goal.position);
        Vector3 direction = goal.position - transform.position;
        Debug.DrawRay(transform.position, direction, Color.red);
        if (direction.magnitude > accuracy)
        {
            transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
        }
    }
}
